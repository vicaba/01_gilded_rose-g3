<?php

namespace G3GildedRose\Item\ConcreteItem;

use G3GildedRose\Item\SellableItem;

class Sulfuras extends SellableItem {

    const ITEM_NAME = "Sulfuras, Hand of Ragnaros";

    protected function setSaleStatus() {
        return;
    }
}