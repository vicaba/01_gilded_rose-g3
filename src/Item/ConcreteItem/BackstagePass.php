<?php

namespace G3GildedRose\Item\ConcreteItem;

use G3GildedRose\Item\SellableItem;

class BackstagePass extends SellableItem
{
    const FARTHEST_EXPIRATION_DATE = 10;

    const FARTHEST_EXPIRATION_DATE_QUALITY_MODIFIER = 2;

    const CLOSEST_EXPIRATION_DATE = 5;

    const CLOSEST_EXPIRATION_DATE_QUALITY_MODIFIER = 3;

    const ITEM_NAME = "Backstage passes to a TAFKAL80ETC concert";

    private function isInFarthestExpirationDate()
    {
        return $this->getSellIn() <= self::FARTHEST_EXPIRATION_DATE;
    }

    protected function isInClosestExpirationDate()
    {
        return $this->getSellIn() <= self::CLOSEST_EXPIRATION_DATE;
    }

    protected function setSaleStatus()
    {
        $this->decreaseSellIn(self::DEFAULT_SELL_IN_MODIFIER);

        if ($this->isSellInExpired()) {
            $this->setQuality(self::NO_QUALITY);
            return;
        }

        if ($this->isInClosestExpirationDate()) {
            $this->increaseQuality(self::CLOSEST_EXPIRATION_DATE_QUALITY_MODIFIER);
            return;
        }
        if ($this->isInFarthestExpirationDate()) {
            $this->increaseQuality(self::FARTHEST_EXPIRATION_DATE_QUALITY_MODIFIER);
            return;
        }
        $this->increaseQuality(self::DEFAULT_QUALITY_MODIFIER);
    }
} 