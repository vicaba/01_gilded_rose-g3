<?php

namespace G3GildedRose\Item\ConcreteItem;

use G3GildedRose\Item\SellableItem;

class AgedBrie extends SellableItem
{

    const ITEM_NAME = "Aged Brie";

    protected function setSaleStatus()
    {
        $this->increaseQuality(self::DEFAULT_QUALITY_MODIFIER);
        $this->decreaseSellIn(self::DEFAULT_SELL_IN_MODIFIER);
    }
} 