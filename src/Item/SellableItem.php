<?php

namespace G3GildedRose\Item;

class SellableItem extends Item
{
    const MAX_ITEM_QUALITY = 50;

    const MIN_ITEM_QUALITY = 0;

    const MIN_ITEM_SELL_IN = 0;

    const NO_QUALITY = 0;

    const DEFAULT_QUALITY_MODIFIER = 1;

    const DEFAULT_SELL_IN_MODIFIER = 1;

    private $decoratedItem = null;

    public function __construct(Item $item)
    {
        $this->decoratedItem = $item;
    }

    public function getName()
    {
        return $this->decoratedItem->getName();
    }

    public function setName($name)
    {
        $this->decoratedItem->setName($name);
    }

    public function getSellIn()
    {
        return $this->decoratedItem->getSellIn();
    }

    public function setSellIn($sellIn)
    {
        $this->decoratedItem->setSellIn($sellIn);
    }

    public function getQuality()
    {
        return $this->decoratedItem->getQuality();
    }

    public function setQuality($quality)
    {
        $this->decoratedItem->setQuality($quality);
    }

    protected function increaseQuality($value)
    {
        $currentQuality = $this->getQuality();
        $this->setQuality($currentQuality + $value);
    }

    protected function decreaseQuality($value)
    {
        $currentQuality = $this->getQuality();
        $this->setQuality($currentQuality - $value);
    }

    protected function increaseSellIn($value)
    {
        $currentSellIn = $this->getSellIn();
        $this->setSellIn($currentSellIn + $value);
    }

    protected function decreaseSellIn($value)
    {
        $currentSellIn = $this->getSellIn();
        $this->setSellIn($currentSellIn - $value);
    }

    protected function isQualityInRange()
    {
        return $this->getQuality() >= self::MIN_ITEM_QUALITY && $this->getQuality() <= self::MAX_ITEM_QUALITY;
    }

    protected function isSellInExpired()
    {
        return $this->getSellIn() < self::MIN_ITEM_SELL_IN;
    }

    public function updateSaleStatus()
    {
        if (!$this->isQualityInRange()) {
            return;
        }

        $this->setSaleStatus();
    }

    protected function setSaleStatus()
    {
        $this->decreaseQuality(self::DEFAULT_QUALITY_MODIFIER);
        $this->decreaseSellIn(self::DEFAULT_SELL_IN_MODIFIER);

        if (!$this->isSellInExpired()) {
            return;
        }

        $this->decreaseQuality(self::DEFAULT_QUALITY_MODIFIER);
    }
}