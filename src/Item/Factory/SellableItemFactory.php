<?php

namespace G3GildedRose\Item\Factory;

use G3GildedRose\Item\ConcreteItem\AgedBrie;
use G3GildedRose\Item\ConcreteItem\BackstagePass;
use G3GildedRose\Item\ConcreteItem\Sulfuras;
use G3GildedRose\Item\Item;
use G3GildedRose\Item\SellableItem;

class SellableItemFactory {
    public static function create(Item $item) {
        switch ($item->getName()) {
            case AgedBrie::ITEM_NAME:
                return new AgedBrie($item);
            case Sulfuras::ITEM_NAME:
                return new Sulfuras($item);
            case BackstagePass::ITEM_NAME:
                return new BackstagePass($item);
            default:
                return new SellableItem($item);
        }
    }
} 