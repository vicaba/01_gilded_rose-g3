<?php

namespace G3GildedRose;

use G3GildedRose\Item\Factory\SellableItemFactory;

class GildedRose
{

    public function updateQuality(array $allItems) {

        $parsedItems = [];
        foreach ($allItems as $item) {
            $currentItem = SellableItemFactory::create($item);
            $currentItem->updateSaleStatus();
            $parsedItems[] = $currentItem;
        }

        return $parsedItems;
    }
}
